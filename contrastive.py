#!/usr/bin/env python3
#
# Copyright 2020 Carlos Castillo
# Apache 2.0.

import argparse
import os
import sys
import numpy as np

def get_args():
  parser = argparse.ArgumentParser(description = 'This script is for the Dacodes Logic test.')
  parser.add_argument('input_filepath', type = str, help = 'The input file, the first line contains the number of test cases (T), with each of the next T lines containing two integers N and M, for the number of rows and columns, respectively.')
  parser.add_argument('output_filepath', type = str, help = 'The output file will contain T lines. Warning: if the file exists, it will be rewritten.')
  args = parser.parse_args()
  return args

def update_facing(matrix, location, facing):
  new_location = (location[0] + facing[0], location[1] + facing[1])
  if new_location[0] >= matrix.shape[0] or new_location[1] >= matrix.shape[1] or matrix[new_location[0]][new_location[1]] == 0:
    if facing == (0, 1):
      return matrix, location, (1, 0)
    elif facing == (1, 0):
      return matrix, location, (0, -1)
    elif facing == (0, -1):
      return matrix, location, (-1, 0)
    else:
      return matrix, location, (0, 1)
  else:
    return matrix, location, facing

def update_location(matrix, location, facing):
  matrix[location[0]][location[1]] = 0
  location = (location[0] + facing[0], location[1] + facing[1])
  return matrix, location, facing

def facing_to_letter(facing):
  if facing == (0, 1):
    return 'R'
  elif facing == (1, 0):
    return 'D'
  elif facing == (0, -1):
    return 'L'
  else:
    return 'U'

def logic(N, M):
  matrix = np.ones([N, M])
  location = (0, 0)
  facing = (0, 1)
  while not np.sum(matrix) == 0:
      last_facing = facing
      matrix, location, facing = update_facing(matrix, location, facing)
      matrix, location, facing = update_location(matrix, location, facing)
  return facing_to_letter(last_facing)

def main():
  args = get_args()
  if not os.path.isfile(args.input_filepath):
    sys.exit(args.input_filepath + ' not found.')
  if os.path.isdir(args.output_filepath):
    sys.exit(args.output_filepath + ' must not be a directory.')
  if os.path.isfile(args.output_filepath):
    print('WARNING: ' + args.output_filepath + ' will be rewritten.')

  f = open(args.input_filepath, 'r')
  input_lines = [line.strip() for line in f.readlines()]
  f.close()
  try:
    T = int(input_lines[0])
    input_tests = [[int(value) for value in N_M.split()] for N_M in input_lines[1:T + 1]]
  except:
    sys.exit(args.input_filepath + ' has wrong format.')

  f = open(args.output_filepath, 'w')
  for N, M in input_tests:
    f.write(logic(N, M) + '\n')
  f.close()

if __name__ == '__main__':
    main()
