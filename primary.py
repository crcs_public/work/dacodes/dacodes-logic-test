#!/usr/bin/env python3
#
# Copyright 2020 Carlos Castillo
# Apache 2.0.

import argparse
import os
import sys

def get_args():
  parser = argparse.ArgumentParser(description = 'This script is for the Dacodes Logic test.')
  parser.add_argument('input_filepath', type = str, help = 'The input file, the first line contains the number of test cases (T), with each of the next T lines containing two integers N and M, for the number of rows and columns, respectively.')
  parser.add_argument('output_filepath', type = str, help = 'The output file will contain T lines. Warning: if the file exists, it will be rewritten.')
  args = parser.parse_args()
  return args

def logic(N, M):
  # To solve the test, I started by performing runs for varying M and N on a whiteboard.
  # The problem can be split into three categories: (1) squares, (2) wide rectangles, and (3) tall rectangles.
  # (1) N == M, the odd squares can be simplified into a 1 x 1 square (a 3 x 3 square is just a wrap around the 1 x 1); the same is true for the odd squares; they can be simplified into a 2 x 2 square.
  # We know the 1 x 1 square result is 'R', and the 2 x 2 is 'L'.
  # The same principle is used to solve for (2) and (3) categories.
  if N <= M:
    if N % 2 == 0:
      return 'L'
    else:
      return 'R'
  else:
    if M % 2 == 0:
      return 'U'
    else:
      return 'D'


def main():
  args = get_args()
  if not os.path.isfile(args.input_filepath):
    sys.exit(args.input_filepath + ' not found.')
  if os.path.isdir(args.output_filepath):
    sys.exit(args.output_filepath + ' must not be a directory.')
  if os.path.isfile(args.output_filepath):
    print('WARNING: ' + args.output_filepath + ' will be rewritten.')

  f = open(args.input_filepath, 'r')
  input_lines = [line.strip() for line in f.readlines()]
  f.close()
  try:
    T = int(input_lines[0])
    input_tests = [[int(value) for value in N_M.split()] for N_M in input_lines[1:T + 1]]
  except:
    sys.exit(args.input_filepath + ' has wrong format.')

  f = open(args.output_filepath, 'w')
  for N, M in input_tests:
    f.write(logic(N, M) + '\n')
  f.close()

if __name__ == '__main__':
    main()
